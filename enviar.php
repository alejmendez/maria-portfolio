<?php

function get($varName) {
    return  isset($_POST[$varName])  ? $_POST[$varName] : 
            (isset($_GET[$varName]) ? $_GET[$varName]  : '');
}

$name    = get('name');
$email   = get('email');
$subject = get('subject');
$message = get('message');

$destinatario = 'camporamaria@gmail.com';
$asunto = 'Este mensaje es de prueba';
$cuerpo = '
<html>
    <head>
        <title>Contacto mariagrafico.cl</title>
    </head>
    <body>
        <h1>Hola maria!</h1>
        <p>
            La siguiente persona quiere contactarte:
        </p>
        <p>
            Nombre:  <b>' . $name . '</b><br />
            Email:   <b>' . $email . '</b><br />
            Asunto:  <b>' . $subject . '</b><br />
            Mensaje: <b>' . $message . '</b>
        </p>
    </body>
</html>
';

//para el envío en formato HTML
$headers = "MIME-Version: 1.0\r\n";
$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";

//dirección del remitente
$headers .= "From: No responder <noreply@mariagrafico.cl>\r\n";

//dirección de respuesta, si queremos que sea distinta que la del remitente
$headers .= "Reply-To: noreply@mariagrafico.cl\r\n";

//ruta del mensaje desde origen a destino
$headers .= "Return-path: noreply@mariagrafico.cl\r\n";

mail($destinatario, $asunto, $cuerpo, $headers);

echo 'success';